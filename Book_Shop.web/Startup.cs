﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Book_Shop.web.Startup))]
namespace Book_Shop.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
