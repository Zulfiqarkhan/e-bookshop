﻿using Book_Shop.Entities;
using Book_Shop.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Book_Shop.web.ViewModel
{
    public class CheckoutViewModel
    {
        public List<Product> Cartproducts { get; set; }
        public List<int> CartproductIDs { get; set; }
        public BookshopUser User { get; set; }
        public List<Order> order { get; set; }
    }

    public class ShopViewModel
    {


        public int MaximamPrice { get; set; }
        public List<Category> FeaturedCategories { get; set; }
        public List<Product> Products { get; set; }
        public int? sortby { get; set; }
        public int? categoryID { get; set; }
        public Pager pager { get; set; }
        public string searchTerm { get; set; }
    }

    public class FilterProductsViewModel
    {
        public List<Product> Products { get; set; }
        public Pager pager { get; set; }
        public int? sortby { get; set; }
        public int? categoryID { get; set; }
        public string searchTerm { get; set; }
    }
}