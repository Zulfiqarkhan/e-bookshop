﻿using Book_Shop.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Book_Shop.web.ViewModel
{
    public class ProductSearchViewModel
    {
        public string PageNo { get; set; }
        public List<Product> Products { get; set; }
        public string SearchTerm { get; set; }
        public Pager Pager { get; set; }
    }
    public class NewProductViewModel
    {
        [Required]
        [MinLength(3), MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        [Range(1, 100000)]
        public decimal Price { get; set; }

       [Required]
        public decimal ISBNno { get; set; }

        [Required]
        public decimal Edition { get; set; }

        [Range(1, 100000)]
        public decimal ProductStock { get; set; }

        [Required]
        [MinLength(3), MaxLength(50)]
        public string Author { get; set; }

        [Required]
        [MinLength(3), MaxLength(50)]
        public string Publisher { get; set; }

        [Range(1, 100000)]
        public decimal OldPrice { get; set; }

        public int CategoryID { get; set; }

        public string ImageUrl { get; set; }

        public List<Category> AvailableCategories { get; set; }
    }

    public class EditProductViewModel
    {
        public int ID { get; set; }
        [Required]
        [MinLength(3), MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        [Required]
        public decimal ISBNno { get; set; }
        [Required]
        public decimal Edition { get; set; }
        [Range(1, 100000)]
        public decimal ProductStock { get; set; }
        [Required]
        [MinLength(3), MaxLength(50)]
        public string Author { get; set; }
        [Required]
        [MinLength(3), MaxLength(50)]
        public string Publisher { get; set; }
        [Range(1, 100000)]
        public decimal OldPrice { get; set; }
        [Range(1, 100000)]
        public decimal Price { get; set; }
        public int CategoryID { get; set; }
        public string ImageUrl { get; set; }
        public List<Category> AvailableCategories { get; set; }
    }
    public class DetailProductViewModel
    {
        public Product Products { get; set; }
    }
}