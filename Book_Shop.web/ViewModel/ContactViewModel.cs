﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Book_Shop.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Book_Shop.web.ViewModel
{
    [Table("Contacts")]
    public class ContactViewModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
      
        public string Email { get; set; }
       
        public string Message { get; set; }
      
        public string PhoneNumber { get; set; }
        public List<Contact> Contacts { get; set; }
        public Pager Pager { get; set; }
    }
}