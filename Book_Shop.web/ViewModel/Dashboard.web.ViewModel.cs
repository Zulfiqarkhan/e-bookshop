﻿using Book_Shop.database;
using Book_Shop.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Book_Shop.web.ViewModel
{
    public class DashboardViewModel
    {
        public int UserCount { get; set; }
        public int Product { get; set; }
        public int Order { get; set; }
        public int Neworder { get; set; }
        public int InProgress { get; set; }
        public int Delivered { get; set; }
        public int Category { get; set; }
        public int Contact { get; set; }
    }
    public class UserViewModel
    {
        public string SearchTerm { get; set; }
        public string RoleID { get; set; }

        public List<IdentityRole> Role{ get; set; }
        public int pageno { get; set; }
        public Pager pager { get; set; }
        public List<BookshopUser> User { get; set; }
    }

    public class UserRoleViewModel
    {
        public List<IdentityRole> UserRole { get; set; }
        public List<IdentityRole> AvailableRole { get; set; }
        public BookshopUser User { get; internal set; }
    }
    public class UserDetailsViewModel
    {
        public BookshopUser User { get; set; }
        public BookshopUser contact { get; set; }
    }

}