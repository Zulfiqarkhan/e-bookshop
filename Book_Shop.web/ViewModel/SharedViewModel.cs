﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Book_Shop.web.ViewModel
{
    public class BaseListingViewModel
    {
    }
    public class Pager
    {
        public Pager(int totalItems, int? page, int pageSize = 10)
        {
            if (pageSize == 0) pageSize = 10;

            var totalpage = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);
            var currentpage = page != null ? (int)page : 1;
            var startPage = currentpage - 5;
            var endPage = currentpage + 4;

            if (startPage <= 0)
            {
                endPage -= (startPage - 1);
                startPage = 1;
            }
            if (endPage > totalpage)
            {
                endPage = totalpage;
                if (endPage > 10)
                {
                    startPage = endPage - 9;
                }
            }
            TotalItems = totalItems;
            CurrentPage = currentpage;
            PageSize = pageSize;
            TotalPages = totalpage;
            StartPage = startPage;
            EndPage = endPage;

        }

        public int TotalItems { get; private set; }
        public int CurrentPage { get; private set; }
        public int PageSize { get; private set; }
        public int TotalPages { get; private set; }
        public int StartPage { get; private set; }
        public int EndPage { get; private set; }
    }
}