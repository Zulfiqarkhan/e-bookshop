﻿using Book_Shop.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Book_Shop.web.ViewModel
{
    public class ProductWidgetsViewModel
    {
        public List<Product> Products { get; set; }
        public bool IslatestProduct { get; set; }
        public bool CategoryID { get; set; }

    }
}