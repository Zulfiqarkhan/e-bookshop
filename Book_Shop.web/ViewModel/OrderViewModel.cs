﻿using Book_Shop.Entities;
using Book_Shop.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Book_Shop.web.ViewModel
{
    public class OrderViewModel
    {
        public List<Order> Order { get; set; }
        public string UserID { get; set; }
        
        public Pager Pager { get; set; }
        public string Status { get; set; }
        public List<BookshopUser> User { get; set; }
    }

    public class UserOrdersViewModel
    {
        public List<Order> Order { get; set; }
        public string UserID { get; set; }
        public Pager Pager { get; set; }
        public string Status { get; set; }
        public BookshopUser Id { get; set; }
        public OrderItem orderitem { get; set; }
        public List<Contact> contacts { get; set; }
    }
    public class OrderDetailViewModel
    {
        public Order Order { get; set; }
        public BookshopUser Orderby { get; set; }
        public List<string> AvailableStatuses { get; set; }
    }
}