﻿using Book_Shop.Services;
using Book_Shop.web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Book_Shop.web.Controllers
{
    public class WidgetsController : Controller
    {
        // GET: Widgets
        public ActionResult Products(bool IslatestProduct, int? CategoryID = 0)
        {
            ProductWidgetsViewModel model = new ProductWidgetsViewModel();
            model.IslatestProduct = IslatestProduct;
            if (IslatestProduct)
            {
                model.Products = ProductServices.Instance.GetlatestProduct(4);
            }
            else if (CategoryID.HasValue && CategoryID.Value > 0)
            {
                model.Products = ProductServices.Instance.GetProductbyCategory(CategoryID.Value, 4);

            }
            else
            {
                model.Products = ProductServices.Instance.GetBestProduct(1, 8);
            }
            return PartialView(model);
        }
    }
}