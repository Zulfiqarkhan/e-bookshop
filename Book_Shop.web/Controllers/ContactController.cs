﻿using Book_Shop.database;
using Book_Shop.Entities;
using Book_Shop.Services;
using Book_Shop.web.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Book_Shop.web.Controllers
{
    public class ContactController : Controller
    {

        // GET: Contact
        public ActionResult Index()
        {
           
                return View();
            
        }

        public ActionResult ContactTable(string userID /*string search, int? pageNo*/)
        {
           using(var context = new CBContext())
            {
                return PartialView(context.Contacts.ToList());
            }
            
        }



        public ActionResult Create()
        {
            return View(new Contact());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Contact model)
        {
            using (var context = new CBContext())
            {
                if (ModelState.IsValid)
                {
                    context.Contacts.Add(model);
                    await context.SaveChangesAsync();
                    return RedirectToAction("Index","home");
                }
                return View(model);
            }
        }




        //public ActionResult Profile()
        //{
        //    return View(new Contact());
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Profile(Contact model)
        //{
        //    using (var context = new CBContext())
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            context.Contacts.Add(model);
        //            await context.SaveChangesAsync();//Save data to sql database
        //            return RedirectToAction("Index");
        //        }
        //        return View(model);
        //    }
        //}

        //public ActionResult Edit(int ID)
        //{
        //    var contact = ContactServices.Instance.GetContact(ID);
        //    return PartialView(contact);
        //}
        //[HttpPost]
        //public ActionResult Edit(Contact contact)
        //{
        //    ContactServices.Instance.UpdateContact(contact);
        //    return RedirectToAction("ContactTable");
        //}

        public ActionResult Details(int ID)
        {
            Contact model = new Contact();

            model = ContactServices.Instance.GetContactbyID(ID);

            return View(model);

        }
      
        public ActionResult Delete(int ID)
        {
            ContactServices.Instance.DeleteContact(ID);
            return RedirectToAction("index");
        }


    }
}