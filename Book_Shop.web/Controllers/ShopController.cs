﻿using Book_Shop.Entities;
using Book_Shop.Services;
using Book_Shop.web.ViewModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Book_Shop.web.Controllers
{
    public class ShopController : Controller
    {
        private BookshopSignInManager _signInManager;
        private BookshopUserManager _userManager;
        public BookshopSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<BookshopSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        public BookshopUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<BookshopUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        public ActionResult Index(string searchTerm, int? minimumPrice, int? maximaumPrice, int? categoryID, int? sortby, int? pageNo)
        {
            ShopViewModel model = new ShopViewModel();
            var PageSize = ConfigurationServices.Instance.ShopPageSize();

            model.FeaturedCategories = CategoryServices.Instance.GetShopCategory();
            model.MaximamPrice = ProductServices.Instance.GetMaximumPrice();
            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;
            model.searchTerm = searchTerm;
            model.sortby = sortby;
            model.categoryID = categoryID;
              
            var totalCounts = ProductServices.Instance.SearchProductsCount(searchTerm, minimumPrice, maximaumPrice, categoryID, sortby);
            model.Products = ProductServices.Instance.SearchProducts(searchTerm, minimumPrice, maximaumPrice, categoryID, sortby, pageNo.Value, PageSize);
            model.pager = new Pager(totalCounts, pageNo, PageSize);
            return View(model);
        }



        public ActionResult FilterProducts(string searchTerm, int? minimumPrice, int? maximaumPrice, int? categoryID, int? sortby, int? pageNo)
        {
            FilterProductsViewModel model = new FilterProductsViewModel();
            var PageSize = ConfigurationServices.Instance.ShopPageSize();
            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;
            model.searchTerm = searchTerm;
            model.sortby = sortby;
            model.categoryID = categoryID;

            var totalCounts = ProductServices.Instance.SearchProductsCount(searchTerm, minimumPrice, maximaumPrice, categoryID, sortby);
            model.Products = ProductServices.Instance.SearchProducts(searchTerm, minimumPrice, maximaumPrice, categoryID, sortby, pageNo.Value, PageSize);
            model.pager = new Pager(totalCounts, pageNo, PageSize);
            return PartialView(model);
        }
        // GET: Shop
        [Authorize]
        public ActionResult Checkout()
        {
            CheckoutViewModel model = new CheckoutViewModel();
            var cartproductcookies = Request.Cookies["CartProducts"];
            if (cartproductcookies != null && !string.IsNullOrEmpty(cartproductcookies.Value))
            {
                model.CartproductIDs = cartproductcookies.Value.Split('-').Select(x => int.Parse(x)).ToList();
                model.Cartproducts = ProductServices.Instance.GetProductlist(model.CartproductIDs);
                model.User = UserManager.FindById(User.Identity.GetUserId());
            }
            return View(model);
        }
        public JsonResult PlaceOrder(string productIDs)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            if (!string.IsNullOrEmpty(productIDs))
            {
                var productQuantities = productIDs.Split('-').Select(x => int.Parse(x)).ToList();

                var BoughtProduct = ProductServices.Instance.GetProductlist(productQuantities.Distinct().ToList());

                Order neworder = new Order();
                neworder.UserID = User.Identity.GetUserId();
                neworder.OrderedAt = DateTime.Now;
                neworder.Status = "Pending";
                neworder.TotalAmount = BoughtProduct.Sum(x => x.Price * productQuantities.Where(productID => productID == x.ID).Count());

                neworder.OrderItem = new List<OrderItem>();
                neworder.OrderItem.AddRange(BoughtProduct.Select(x => new OrderItem() { ProductID = x.ID, Quantity = productQuantities.Where(productID => productID == x.ID).Count() }));
                var rowsEffected = ShopServices.Instance.SaveOrder(neworder);

                result.Data = new { Success = true };
            }
            else
            {
                result.Data = new { Success = false };

            }
            return result;
        }


        [Authorize]
        public ActionResult UserOrders(string userID, string status, int? pageNo)
        {
            UserOrdersViewModel model = new UserOrdersViewModel();
            model.UserID = userID;
            model.Status = status;
            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;
            var pageSize = ConfigurationServices.Instance.CategoryPageSize();

            model.Order = OrderServices.Instance.SearchOrders(userID, status, pageNo.Value, pageSize);
            var totalRecord = OrderServices.Instance.SearchOrderscount(userID, status);
            model.Pager = new Pager(totalRecord, pageNo, pageSize);

            return View(model);
        }

        public ActionResult DetailsUserOrder(int ID)
        {
            OrderDetailViewModel model = new OrderDetailViewModel();
            model.Order = OrderServices.Instance.GetOrderbyID(ID);
            if (model.Order != null)
            {
                model.Orderby = UserManager.FindById(model.Order.UserID);
            }
            model.AvailableStatuses = new List<string>() { "Pending", "In Progress", "Delivered" };

            return View(model);
        }
    }
}