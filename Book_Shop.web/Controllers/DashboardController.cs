﻿using Book_Shop.database;
using Book_Shop.Entities;
using Book_Shop.Services;
using Book_Shop.web.ViewModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Book_Shop.web.Controllers
{
    public class DashboardController : Controller
    {
        DashboardServices services = new DashboardServices();

        private BookshopUserManager _userManager;
        private BookshopRoleManager _roleManager;

        public DashboardController()
        {
        }

        public DashboardController(BookshopUserManager userManager, BookshopRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = RoleManager;
        }

        public BookshopUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<BookshopUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public BookshopRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<BookshopRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }


        // GET: Dashboard
        public ActionResult Index()
        {
            DashboardViewModel model = new DashboardViewModel();
            model.UserCount = services.GetUserCount();
            model.Product = services.GetProductCount();
            model.Order = services.GetOrderCount();
            model.Neworder = services.GetNewOrderCount();
            model.InProgress = services.GetInProgressOrderCount();
            model.Delivered = services.GetDeliverdOrderCount();
            model.Category = services.GetCategoryCount();
            model.Contact = services.GetContactCount();

            return View(model);
        }

        public new ActionResult User()
        {
            return View();
        }

        public ActionResult UserTable(string roleID, string searchTerm, int? pageNo)
        {
            var pagesize = ConfigurationServices.Instance.CategoryPageSize();
            UserViewModel model = new UserViewModel();
            model.RoleID = roleID;
            model.SearchTerm = searchTerm;

            //model.Role = new List<IdentityRole>();
            // model.User = UserManager.Users.ToList();

            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;

            var Users = UserManager.Users;
            if (!string.IsNullOrEmpty(searchTerm))
            {
                Users = Users.Where(x => x.Email.ToLower().Contains(searchTerm.ToLower()));
            }
            pageNo = pageNo ?? 1;
            var skipCount = (pageNo.Value - 1) * pagesize;
            model.User = Users.OrderBy(x => x.Email).Skip(skipCount).Take(pagesize).ToList();

          

            if (model.User != null)
            {
                model.pager = new Pager(Users.Count(), pageNo, pagesize);
                return PartialView(model);
            }
            else
            {
                return HttpNotFound();

            }
        }

        public async Task<ActionResult> UserDetails(string userID, bool isPartial = false)
        {
            UserDetailsViewModel model = new UserDetailsViewModel();
            var user = await UserManager.FindByIdAsync(userID);
            if (user != null)
            {
                model.User = user;
            }
            if (isPartial || Request.IsAjaxRequest())
            {
                return PartialView("_userdetails", model);
            }
            else
            {
                return View(model);
            }
        }
        public async Task<ActionResult> UserRoles(string userID)
        {
            UserRoleViewModel model = new UserRoleViewModel();

            model.AvailableRole = RoleManager.Roles.ToList();
            if (!string.IsNullOrEmpty(userID))
            {
                model.User = await UserManager.FindByIdAsync(userID);

                if (model.User != null)
                {
                    model.UserRole = model.User.Roles.Select(userRole => model.AvailableRole.FirstOrDefault(role => role.Id == userRole.RoleId)).ToList();
                }

            }
            return PartialView("_userRoles", model);

        }

        public async Task<ActionResult> AssignUserRole(string userID, string roleID)
        {

            if (!string.IsNullOrEmpty(userID) && !string.IsNullOrEmpty(roleID))
            {
                var user = await UserManager.FindByIdAsync(userID);

                if (user != null)
                {
                    var role = await RoleManager.FindByIdAsync(roleID);

                    if (role != null)
                    {
                        await UserManager.AddToRoleAsync(userID, role.Name);

                    }
                }

            }
            return RedirectToAction("userRoles", new { userID = userID });

        }

        public async Task<ActionResult> DeleteuserRole(string userID, string roleID)
        {

            if (!string.IsNullOrEmpty(userID) && !string.IsNullOrEmpty(roleID))
            {
                var user = await UserManager.FindByIdAsync(userID);

                if (user != null)
                {
                    var role = await RoleManager.FindByIdAsync(roleID);

                    if (role != null)
                    {
                        await UserManager.RemoveFromRoleAsync(userID, role.Name);

                    }
                }

            }
            return RedirectToAction("userRoles", new { userID = userID });

        }

        public ActionResult UserOrdersforAdmin(string userID, string status, int? pageNo)
        {
            UserOrdersViewModel model = new UserOrdersViewModel();
            model.UserID = userID;
            model.Status = status;



            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;
            var pageSize = ConfigurationServices.Instance.CategoryPageSize();


            model.Order = OrderServices.Instance.SearchOrders(userID, status, pageNo.Value, pageSize);
            var totalRecord = OrderServices.Instance.SearchOrderscount(userID, status);
            model.Pager = new Pager(totalRecord, pageNo, pageSize);

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Deleteorder(int ID)
        {
            OrderServices.Instance.DeleteOrder(ID);
            return RedirectToAction("Userordersforadmin");
        }

        public ActionResult Role()
        {
            return View();
        }

      

        public ActionResult RoleTable(string roleID, string searchTerm, int? pageNo)
        {
            var pagesize = ConfigurationServices.Instance.CategoryPageSize();
            UserViewModel model = new UserViewModel();
            model.RoleID = roleID;
            model.SearchTerm = searchTerm;

            //model.Role = new List<IdentityRole>();
            // model.User = UserManager.Users.ToList();

            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;

            var Users = UserManager.Users;
            if (!string.IsNullOrEmpty(searchTerm))
            {
                Users = Users.Where(x => x.Email.ToLower().Contains(searchTerm.ToLower()));
            }
            pageNo = pageNo ?? 1;
            var skipCount = (pageNo.Value - 1) * pagesize;
            model.User = Users.OrderBy(x => x.Email).Skip(skipCount).Take(pagesize).ToList();

            if (model.User != null)
            {
                model.pager = new Pager(Users.Count(), pageNo, pagesize);
                return PartialView(model);
            }
            else
            {
                return HttpNotFound();

            }
        }

        public ActionResult RoleDetails()
        {
            return View();
        }

        public  ActionResult UserRoleDetails(string userID)
        {
            UserRoleViewModel model = new UserRoleViewModel();

            model.AvailableRole = RoleManager.Roles.ToList();
            if (!string.IsNullOrEmpty(userID))
            {
                model.User =  UserManager.FindById(userID);

                if (model.User != null)
                {
                    model.UserRole = model.User.Roles.Select(userRole => model.AvailableRole.FirstOrDefault(role => role.Id == userRole.RoleId)).ToList();
                }

            }
            return PartialView(model);

        }

        public  ActionResult AssignRole(string userID, string roleID)
        {

            if (!string.IsNullOrEmpty(userID) && !string.IsNullOrEmpty(roleID))
            {
                var user = UserManager.FindById(userID);

                if (user != null)
                {
                    var role =  RoleManager.FindById(roleID);

                    if (role != null)
                    {
                         UserManager.AddToRole(userID, role.Name);

                    }
                }

            }
            return RedirectToAction("UserRoleDetails", new { userID = userID });

        }

        public ActionResult DeleteRole(string userID, string roleID)
        {

            if (!string.IsNullOrEmpty(userID) && !string.IsNullOrEmpty(roleID))
            {
                var user = UserManager.FindById(userID);

                if (user != null)
                {
                    var role = RoleManager.FindById(roleID);

                    if (role != null)
                    {
                        UserManager.RemoveFromRole(userID, role.Name);

                    }
                }

            }
            return RedirectToAction("userRoleDetails", new { userID = userID });

        }

        public ActionResult UserContactsforAdmin(string userID)
        {
           ContactViewModel model = new ContactViewModel();
            model.Contacts = OrderServices.Instance.GetcontactforAdmin(userID);
            //model.UserID = userID;
            //model.Status = status;



            //pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;
            //var pageSize = ConfigurationServices.Instance.CategoryPageSize();


            //model.Order = OrderServices.Instance.SearchOrders(userID, status, pageNo.Value, pageSize);
            //var totalRecord = OrderServices.Instance.SearchOrderscount(userID, status);
            //model.Pager = new Pager(totalRecord, pageNo, pageSize);

            return PartialView(model);
        }
    }
}