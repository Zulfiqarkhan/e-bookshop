﻿using Book_Shop.Services;
using Book_Shop.web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Book_Shop.web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Homeviewmodel model = new Homeviewmodel();
            model.FeatureCategories = CategoryServices.Instance.GetFeaturedCategory();
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}