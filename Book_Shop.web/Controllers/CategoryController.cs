﻿using Book_Shop.Entities;
using Book_Shop.Services;
using Book_Shop.web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Book_Shop.web.Controllers
{
        // GET: Category
        public class CategoryController : Controller
        {
            //CategoryServices CatServices = new CategoryServices();
            // GET: Category
            public ActionResult Index()
            {
                return View();
            }
            public ActionResult CategoryTable(string search, int? pageNo)
            {
                CategorySearchViewModel model = new CategorySearchViewModel();

                var pagesize = ConfigurationServices.Instance.CategoryPageSize();

                model.SearchTerm = search;
                pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;
                var totalRecord = CategoryServices.Instance.GetCategoryCount(search);
                model.Categories = CategoryServices.Instance.GetCategorieslist(search, pageNo.Value, pagesize);
                if (model.Categories != null)
                {
                    model.Pager = new Pager(totalRecord, pageNo, pagesize);
                    return PartialView("CategoryTable", model);
                }
                else
                {
                    return HttpNotFound();
                }
            }

            public ActionResult Create()
            {
                NewCategoryViewModel model = new NewCategoryViewModel();
                return PartialView(model);
            }
            [HttpPost]
            public ActionResult Create(NewCategoryViewModel model)
            {
                if (ModelState.IsValid)
                {

                    var newcategory = new Category();
                    newcategory.Name = model.Name;
                    newcategory.Description = model.Description;
                    newcategory.ImageUrl = model.ImageUrl;
                    newcategory.IsFeatured = model.IsFeatured;

                    CategoryServices.Instance.CreateCategory(newcategory);
                    return RedirectToAction("CategoryTable");
                }
                else
                {
                    return new HttpStatusCodeResult(500);
                }
            }
            public ActionResult Edit(int ID)
            {
                var category = CategoryServices.Instance.Getcategory(ID);
                return PartialView(category);
            }
            [HttpPost]
            public ActionResult Edit(Category category)
            {
                CategoryServices.Instance.UpdateCategory(category);
                return RedirectToAction("categorytable");
            }
            [HttpPost]
            public ActionResult Delete(int ID)
            {
                CategoryServices.Instance.DeleteCategory(ID);
                return RedirectToAction("categorytable");
            }
        }
    
}