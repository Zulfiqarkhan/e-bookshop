﻿using Book_Shop.Entities;
using Book_Shop.Services;
using Book_Shop.web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Book_Shop.web.Controllers
{
    public class ProductController : Controller
    {
        //ProductServices productservices = new ProductServices();
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProductTable(string search, int? pageNo)
        {
            var pagesize = ConfigurationServices.Instance.ProductPageSize();
            ProductSearchViewModel model = new ProductSearchViewModel();
            model.SearchTerm = search;
            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;
            var totalRecord = ProductServices.Instance.GetProductCount(search);
            model.Products = ProductServices.Instance.GetProductlist(search, pageNo.Value, pagesize);
            if (model.Products != null)
            {
                model.Pager = new Pager(totalRecord, pageNo, pagesize);
                return PartialView(model);
                //model.Products = model.Products.Where(p => p.Name != null && p.Name.ToLower().Contains(search.ToLower())).ToList();
            }
            else
            {
                return HttpNotFound();

            }
        }
        [HttpGet]
        public ActionResult Create()
        {
            //CategoryServices categoryservices = new CategoryServices();
            NewProductViewModel model = new NewProductViewModel();
            model.AvailableCategories = CategoryServices.Instance.GetAllCategorieslist();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult Create(NewProductViewModel model)
        {
            //CategoryServices categoryservices = new CategoryServices();
            var newproduct = new Product();
            newproduct.Name = model.Name;
            newproduct.Description = model.Description;
            newproduct.OldPrice = model.OldPrice;
            newproduct.ISBNno = model.ISBNno;
            newproduct.Edition = model.Edition;
            newproduct.Author = model.Author;
            newproduct.Publisher = model.Publisher;
            newproduct.ProductStock = model.ProductStock;
            newproduct.Price = model.Price;
            newproduct.Category = CategoryServices.Instance.Getcategory(model.CategoryID);
            newproduct.ImageUrl = model.ImageUrl;
            ProductServices.Instance.CreateProduct(newproduct);
            return RedirectToAction("ProductTable");
        }

        public ActionResult Edit(int ID)
        {
            EditProductViewModel model = new EditProductViewModel();

            var product = ProductServices.Instance.Getproduct(ID);

            model.ID = product.ID;
            model.Name = product.Name;
            model.Description = product.Description;
            model.OldPrice = product.OldPrice;
            model.ISBNno = product.ISBNno;
            model.Edition = product.Edition;
            model.Author = product.Author;
            model.Publisher = product.Publisher;
            model.ProductStock = product.ProductStock;
            model.Price = product.Price;
            model.ImageUrl = product.ImageUrl;
            model.CategoryID = product.Category != null ? product.Category.ID : 0;
            model.AvailableCategories = CategoryServices.Instance.GetAllCategorieslist();
            return PartialView(model);

        }
        [HttpPost]
        public ActionResult Edit(EditProductViewModel model)
        {

            var existingproduct = ProductServices.Instance.Getproduct(model.ID);


            existingproduct.Name = model.Name;
            existingproduct.Description = model.Description;
            existingproduct.Price = model.Price;
            existingproduct.ISBNno = model.ISBNno;
            existingproduct.Author = model.Author;
            existingproduct.Publisher = model.Publisher;
            existingproduct.ProductStock = model.ProductStock;
            existingproduct.OldPrice = model.OldPrice;
            existingproduct.ImageUrl = model.ImageUrl;
            existingproduct.Category = null;
            existingproduct.CategoryID = model.CategoryID;
            ProductServices.Instance.UpdateProduct(existingproduct);
            return RedirectToAction("ProductTable");

        }

        [HttpPost]
        public ActionResult Delete(int ID)
        {
            ProductServices.Instance.DeleteProduct(ID);
            return RedirectToAction("producttable");
        }
        public ActionResult Details(int ID)
        {
            DetailProductViewModel model = new DetailProductViewModel();

            model.Products = ProductServices.Instance.Getproduct(ID);

            return View(model);

        }
    }
}