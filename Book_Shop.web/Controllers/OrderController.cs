﻿using Book_Shop.Entities;
using Book_Shop.Services;
using Book_Shop.web.ViewModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Book_Shop.web.Controllers
{
    public class OrderController : Controller
    {
        private BookshopUserManager _userManager;
        private BookshopRoleManager _roleManager;

        public OrderController()
        {
        }

        public OrderController(BookshopUserManager userManager, BookshopRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = RoleManager;
        }

        public BookshopUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<BookshopUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public BookshopRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<BookshopRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        // GET: Order


        public ActionResult OrderView()
        {
            return View();
        }
        public ActionResult Index(string userID, string status, int? pageNo)
        {
            OrderViewModel model = new OrderViewModel();
            model.UserID = userID;
            model.Status = status;
         
            

            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;
            var pageSize = ConfigurationServices.Instance.CategoryPageSize();


            model.Order = OrderServices.Instance.SearchOrders(userID, status, pageNo.Value, pageSize);
            var totalRecord = OrderServices.Instance.SearchOrderscount(userID, status);
            //model.User = UserManager.Users.ToList();
            model.Pager = new Pager(totalRecord, pageNo, pageSize);

            return PartialView(model);
        }

        public ActionResult Details(int ID)
        {
            OrderDetailViewModel model = new OrderDetailViewModel();
            model.Order = OrderServices.Instance.GetOrderbyID(ID);
            if (model.Order != null)
            {
                model.Orderby = UserManager.FindById(model.Order.UserID);
            }
            model.AvailableStatuses = new List<string>() { "Pending", "In Progress", "Delivered" };

            return View(model);
        }

       
        public JsonResult ChangeStatus(string status, int ID)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = new { Success = OrderServices.Instance.UpdateOrderStatus(ID, status) };
            return result;
        }

        [HttpPost]
        public ActionResult Deleteorder(int ID)
        {
            OrderServices.Instance.DeleteOrder(ID);
            return RedirectToAction("index");
        }
      
        public ActionResult LogedUser()
        {
            return View();
        }


        [Authorize]
        public ActionResult LogedInUser()
        {
            BookshopUser model = new BookshopUser();
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                model.Id = user.Id;
                model.Name = user.Name;
                model.UserName = user.UserName;
                model.Email = user.Email;
                model.City = user.City;
                model.Address = user.Address;
                model.PhoneNumber = user.PhoneNumber;



            }
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult LogedInUser(BookshopUser user)
        {
            OrderServices.Instance.UpdateUser(user);
            return RedirectToAction("LogedInUser");
        }
    }
}