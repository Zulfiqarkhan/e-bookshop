﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Book_Shop.web.Code
{
    public enum SortbyEnum
    {
        Default = 1,
        Popularity = 2,
        Pricelowtohigh = 3,
        Pricehightolow = 4,
    }
}