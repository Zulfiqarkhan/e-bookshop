﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.Entities
{
    public class Category : BaseEntity
    {
        public string ImageUrl { get; set; }

        public bool IsFeatured { get; set; }
        public List<Product> Product { get; set; }
    }
}
