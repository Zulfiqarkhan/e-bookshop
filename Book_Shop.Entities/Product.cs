﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.Entities
{
    public class Product : BaseEntity
    {
        [Required]
        public string ImageUrl { get; set; }

        [Required]
        [Range(1, 100000)]
        public decimal Price { get; set; }

        [Required]
        [Range(1, 100000)]
        public decimal OldPrice { get; set; }

        [Required]
        public decimal ISBNno { get; set; }

        [Required]
        public decimal Edition { get; set; }

        [Required]
        [Range(1, 100000)]
        public decimal ProductStock { get; set; }

        [Required]
        [MinLength(3), MaxLength(50)]
        public string Author { get; set; }

        [Required]
        [MinLength(3), MaxLength(50)]
        public string Publisher { get; set; }

        public int CategoryID { get; set; }
        public virtual Category Category { get; set; }
    }
}
