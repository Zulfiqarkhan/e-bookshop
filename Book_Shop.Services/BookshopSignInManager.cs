﻿using Book_Shop.Entities;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.Services
{
    public class BookshopSignInManager : SignInManager<BookshopUser, string>
    {
        public BookshopSignInManager(BookshopUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(BookshopUser user)
        {
            return user.GenerateUserIdentityAsync((BookshopUserManager)UserManager);
        }

        public static BookshopSignInManager Create(IdentityFactoryOptions<BookshopSignInManager> options, IOwinContext context)
        {
            return new BookshopSignInManager(context.GetUserManager<BookshopUserManager>(), context.Authentication);
        }
    }
}
