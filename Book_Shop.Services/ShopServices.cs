﻿using Book_Shop.database;
using Book_Shop.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.Services
{
    public class ShopServices
    {
        #region Singleton
        private static ShopServices instance { get; set; }
        public static ShopServices Instance
        {
            get
            {
                if (instance == null) instance = new ShopServices();
                return instance;
            }
        }


        private ShopServices()
        {
        }


        #endregion
        public int SaveOrder(Order order)
        {
            using (var context = new CBContext())
            {

                context.Orders.Add(order);
                return context.SaveChanges();
            }
        }

        public List<Category> GetShopCategory()
        {
            using (var context = new CBContext())
            {
                return context.Categories.ToList();
            }
        }
    }
}
