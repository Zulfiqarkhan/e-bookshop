﻿using Book_Shop.database;
using Book_Shop.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.Services
{
    public class BookshopRoleManager : RoleManager<IdentityRole>
    {
        public BookshopRoleManager(IRoleStore<IdentityRole, string> roleStore): base(roleStore)
        {
        }

        public static BookshopRoleManager Create(IdentityFactoryOptions<BookshopRoleManager> options, IOwinContext context)
        {
            return new BookshopRoleManager(new RoleStore<IdentityRole>(context.Get<CBContext>()));
        }
    }
}
