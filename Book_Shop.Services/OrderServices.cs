﻿using Book_Shop.database;
using Book_Shop.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.Services
{
    public class OrderServices
    {
        #region Singleton
        private static OrderServices instance { get; set; }
        public static OrderServices Instance
        {
            get
            {
                if (instance == null) instance = new OrderServices();
                return instance;
            }
        }


        private OrderServices()
        {
        }


        #endregion
        public List<Order> SearchOrders(string userID, string status, int pageNo, int pageSize)
        {

            using (var context = new CBContext())
            {
                var orders = context.Orders.ToList();

                if (!string.IsNullOrEmpty(userID))
                {
                    orders = orders.Where(x => x.UserID.ToLower().Contains(userID.ToLower())).ToList();
                }
                if (!string.IsNullOrEmpty(status))
                {
                    orders = orders.Where(x => x.Status.ToLower().Contains(status.ToLower())).ToList();
                }

                return orders.Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
            }
        }

        //public List<OrderItem> SearchOrderItem(string userID)
        //{

        //    using (var context = new CBContext())
        //    {
        //        var ordersitem = context.OrderItems.ToList();

              
        //        return ordersitem;
        //    }
        //}
        public int SearchOrderscount(string userID, string status)
        {

            using (var context = new CBContext())
            {
                var orders = context.Orders.ToList();

                if (!string.IsNullOrEmpty(userID))
                {
                    orders = orders.Where(x => x.UserID.ToLower().Contains(userID.ToLower())).ToList();
                }
                if (!string.IsNullOrEmpty(status))
                {
                    orders = orders.Where(x => x.Status.ToLower().Contains(status.ToLower())).ToList();
                }

                return orders.Count;
            }
        }
        public Order GetOrderbyID(int ID)
        {

            using (var context = new CBContext())
            {
                return context.Orders.Where(x => x.ID == ID).Include(x => x.OrderItem).Include("Orderitem.Products").FirstOrDefault();
            }
        }

        public bool UpdateOrderStatus(int ID, string status)
        {
            using (var context = new CBContext())
            {
                var order = context.Orders.Find(ID);
                order.Status = status;
                context.Entry(order).State = EntityState.Modified;
                return context.SaveChanges() > 0;
            }
        }

        public void DeleteOrder(int ID)
        {
            using (var context = new CBContext())
            {
                var order = context.Orders.Find(ID);
                context.Entry(order).State = System.Data.Entity.EntityState.Deleted;
                context.SaveChanges();
            }
        }

        public void UpdateUser(BookshopUser user)
        {
            using (var context = new CBContext())
            {
                context.Entry(user).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public List<Contact> GetcontactforAdmin(string userID)
        {

            using (var context = new CBContext())
            {
                var contact = context.Contacts.ToList();

                return contact;
            }
        }
    }
}
