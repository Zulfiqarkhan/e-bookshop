﻿using Book_Shop.database;
using Book_Shop.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.Services
{
    public class ProductServices
    {
        #region Singleton
        private static ProductServices instance { get; set; }
        public static ProductServices Instance
        {
            get
            {
                if (instance == null) instance = new ProductServices();
                return instance;
            }
        }


        private ProductServices()
        {
        }


        #endregion


        public List<Product> SearchProducts(string searchTerm, int? minimumPrice, int? maximaumPrice, int? categoryID, int? Sortby, int pageNo, int pageSize)
        {

            using (var context = new CBContext())
            {
                var products = context.Products.ToList();

                if (categoryID.HasValue)
                {
                    products = products.Where(x => x.Category.ID == categoryID.Value).ToList();
                }

                if (!string.IsNullOrEmpty(searchTerm))
                {
                    products = products.Where(x => x.Name.ToLower().Contains(searchTerm.ToLower())).ToList();
                }
                if (minimumPrice.HasValue)
                {

                    products = products.Where(x => x.Price >= minimumPrice.Value).ToList();
                }
                if (maximaumPrice.HasValue)
                {

                    products = products.Where(x => x.Price <= maximaumPrice.Value).ToList();

                }
                if (Sortby.HasValue)
                {
                    switch (Sortby.Value)
                    {
                        case 3:
                            products = products.OrderBy(x => x.Price).ToList();
                            break;
                        case 4:
                            products = products.OrderByDescending(x => x.Price).ToList();
                            break;
                        default:
                            products = products.OrderByDescending(x => x.ID).ToList();

                            break;
                    }
                }

                return products.Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
            }
        }

        public int SearchProductsCount(string searchTerm, int? minimumPrice, int? maximaumPrice, int? categoryID, int? Sortby)
        {

            using (var context = new CBContext())
            {
                var products = context.Products.ToList();

                if (categoryID.HasValue)
                {
                    products = products.Where(x => x.Category.ID == categoryID.Value).ToList();
                }

                if (!string.IsNullOrEmpty(searchTerm))
                {
                    products = products.Where(x => x.Name.ToLower().Contains(searchTerm.ToLower())).ToList();
                }
                if (minimumPrice.HasValue)
                {

                    products = products.Where(x => x.Price >= minimumPrice.Value).ToList();
                }
                if (maximaumPrice.HasValue)
                {

                    products = products.Where(x => x.Price <= maximaumPrice.Value).ToList();

                }
                if (Sortby.HasValue)
                {
                    switch (Sortby.Value)
                    {
                        case 3:
                            products = products.OrderBy(x => x.Price).ToList();
                            break;
                        case 4:
                            products = products.OrderByDescending(x => x.Price).ToList();
                            break;
                        default:
                            products = products.OrderByDescending(x => x.ID).ToList();

                            break;
                    }
                }

                return products.Count;
            }
        }

        public int GetMaximumPrice()
        {
            using (var context = new CBContext())
            {
                return (int)context.Products.Max(x => x.Price);
            }
        }

        public List<Product> GetProductlist()
        {
            using (var context = new CBContext())
            {
                return context.Products.Include(x => x.Category).ToList();
            }
        }
        public List<Product> GetProductlist(List<int> IDs)
        {
            using (var context = new CBContext())
            {
                return context.Products.Where(product => IDs.Contains(product.ID)).ToList();
            }
        }
        public List<Product> GetProductlist(string search, int pageNo, int pageSize)
        {
            using (var context = new CBContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Products.Where(product => product.Name != null &&
                    product.Name.ToLower().Contains(search.ToLower()))
                    .OrderBy(x => x.ID)
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .Include(x => x.Category)
                    .ToList();
                }
                else
                {
                    return context.Products
                   .OrderBy(x => x.ID)
                   .Skip((pageNo - 1) * pageSize)
                   .Take(pageSize)
                   .Include(x => x.Category)
                   .ToList();

                }
            }
        }

        public int GetProductCount(string search)
        {
            using (var context = new CBContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Products.Where(product => product.Name != null &&
                    product.Name.ToLower().Contains(search.ToLower())).Count();
                }
                else
                {
                    return context.Products.Count();

                }
            }
        }

        public List<Product> GetlatestProduct(int pageNo, int pageSize)
        {
            using (var context = new CBContext())
            {
                return context.Products.OrderByDescending(x => x.ID).Skip((pageNo - 1) * pageSize).Take(pageSize).Include(x => x.Category).ToList();
            }
        }

        public List<Product> GetBestProduct(int pageNo, int pageSize)
        {
            using (var context = new CBContext())
            {
                return context.Products.OrderByDescending(x => x.ID).Skip((pageNo - 1) * pageSize).Take(pageSize).Include(x => x.Category).ToList();
            }
        }
        public List<Product> GetlatestProduct(int numberofproduct)
        {
            using (var context = new CBContext())
            {
                return context.Products.OrderByDescending(x => x.ID).Take(numberofproduct).Include(x => x.Category).ToList();
            }
        }

        public List<Product> GetProductbyCategory(int categoryID, int pageSize)
        {
            using (var context = new CBContext())
            {
                return context.Products.Where(x => x.Category.ID == categoryID).OrderByDescending(x => x.ID).Take(pageSize).Include(x => x.Category).ToList();
            }
        }

        public void CreateProduct(Product product)
        {
            using (var context = new CBContext())
            {
                context.Entry(product.Category).State = System.Data.Entity.EntityState.Unchanged;

                context.Products.Add(product);
                context.SaveChanges();
            }
        }

        public Product Getproduct(int ID)
        {
            using (var context = new CBContext())
            {
                //return context.Products.Find(ID);
                return context.Products.Where(x => x.ID == ID).Include(x => x.Category).FirstOrDefault();

            }

        }

        public void UpdateProduct(Product product)
        {
            using (var context = new CBContext())
            {

                context.Entry(product).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteProduct(int ID)
        {
            using (var context = new CBContext())
            {
                var product = context.Products.Find(ID);
                context.Entry(product).State = System.Data.Entity.EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}
