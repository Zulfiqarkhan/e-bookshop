﻿using Book_Shop.database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.Services
{
   public class DashboardServices
    {
        public int GetUserCount()
        {
            CBContext context = new CBContext();
            return context.Users.Count();
        }
        public int GetProductCount()
        {
            CBContext context = new CBContext();
            return context.Products.Count();
        }
        public int GetOrderCount()
        {
            CBContext context = new CBContext();
            return context.Orders.Count();
        }

        public int GetNewOrderCount()
        {
            CBContext context = new CBContext();
            return context.Orders.Where(x => x.Status == "Pending").Count();
        }
        public int GetInProgressOrderCount()
        {
            CBContext context = new CBContext();
            return context.Orders.Where(x => x.Status == "In Progress").Count();
        }

        public int GetDeliverdOrderCount()
        {
            CBContext context = new CBContext();
            return context.Orders.Where(x => x.Status == "Delivered").Count();
        }

        public int GetCategoryCount()
        {
            CBContext context = new CBContext();
            return context.Categories.Count();
        }
        public int GetContactCount()
        {
            CBContext context = new CBContext();
            return context.Contacts.Count();
        }
    }
}
