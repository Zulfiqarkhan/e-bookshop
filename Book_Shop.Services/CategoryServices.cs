﻿using Book_Shop.database;
using Book_Shop.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.Services
{
    public class CategoryServices
    {
        #region Singleton
        private static CategoryServices instance { get; set; }
        public static CategoryServices Instance
        {
            get
            {
                if (instance == null) instance = new CategoryServices();
                return instance;
            }
        }
        //private CategoryServices()
        //{
        //}
        #endregion
        public Category Getcategory(int ID)
        {
            using (var context = new CBContext())
            {
                return context.Categories.Find(ID);

            }

        }
        public List<Category> GetAllCategorieslist()
        {

            using (var context = new CBContext())
            {
                return context.Categories.ToList();
            }
        }
        public int GetCategoryCount(string search)
        {
            using (var context = new CBContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Categories.Where(category => category.Name != null &&
                    category.Name.ToLower().Contains(search.ToLower())).Count();
                }
                else
                {
                    return context.Categories.Count();

                }
            }
        }
        public List<Category> GetCategorieslist(string search, int pageNo, int pagesize)
        {
            using (var context = new CBContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Categories.Where(category => category.Name != null &&
                    category.Name.ToLower().Contains(search.ToLower()))
                    .OrderBy(x => x.ID)
                    .Skip((pageNo - 1) * pagesize)
                    .Take(pagesize)
                    .Include(x => x.Product)
                    .ToList();
                }
                else
                {
                    return context.Categories
                   .OrderBy(x => x.ID)
                   .Skip((pageNo - 1) * pagesize)
                   .Take(pagesize)
                   .Include(x => x.Product)
                   .ToList();

                }
            }
        }

        public List<Category> GetFeaturedCategory()
        {
            using (var context = new CBContext())
            {
                return context.Categories.OrderByDescending(x=>x.ID).Where(x => x.IsFeatured && x.ImageUrl != null).ToList();
            }
        }
        public List<Category> GetShopCategory()
        {
            using (var context = new CBContext())
            {
                return context.Categories.Where(x => x.ImageUrl != null).ToList();
            }
        }
        public void CreateCategory(Category category)
        {
            using (var context = new CBContext())
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }
        public void UpdateCategory(Category category)
        {
            using (var context = new CBContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

      
        public void DeleteCategory(int ID)
        {
            using (var context = new CBContext())
            {
                var category = context.Categories.Find(ID);
                context.Entry(category).State = System.Data.Entity.EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}
