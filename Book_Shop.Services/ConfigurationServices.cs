﻿using Book_Shop.database;
using Book_Shop.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.Services
{
    public class ConfigurationServices
    {
        #region Singlton ConfigurationServices
        private static ConfigurationServices instance { get; set; }
        public static ConfigurationServices Instance
        {
            get
            {
                if (instance == null) instance = new ConfigurationServices();
                return instance;
            }
        }
        private ConfigurationServices()
        {
        }
        #endregion



        public Config GetConfigs(string key)
        {
            using (var context = new CBContext())
            {
                return context.Configurations.Find(key);
            }
        }
        public int CategoryPageSize()
        {
            using (var context = new CBContext())
            {

                var PageSizeconfig = context.Configurations.Find("PageSize");
                return PageSizeconfig != null ? int.Parse(PageSizeconfig.Value) : 5;
            }
        }
        public int ProductPageSize()
        {
            using (var context = new CBContext())
            {

                var PageSizeconfig = context.Configurations.Find("PageSize");
                return PageSizeconfig != null ? int.Parse(PageSizeconfig.Value) : 10;
            }
        }
        public int ShopPageSize()
        {
            using (var context = new CBContext())
            {

                var PageSizeconfig = context.Configurations.Find("PageSize");
                return PageSizeconfig != null ? int.Parse(PageSizeconfig.Value) : 20;
            }
        }
    }
}
