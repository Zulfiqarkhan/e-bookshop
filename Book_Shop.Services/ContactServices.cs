﻿using Book_Shop.database;
using Book_Shop.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.Services
{
    public class ContactServices
    {
        #region Singleton
        private static ContactServices instance { get; set; }
        public static ContactServices Instance
        {
            get
            {
                if (instance == null) instance = new ContactServices();
                return instance;
            }
        }
        //private CategoryServices()
        //{
        //}
        #endregion


        public Contact GetContact(int ID)
        {
            using (var context = new CBContext())
            {
                return context.Contacts.Find(ID);

            }

        }
        //public List<Contact> GetAllContactlist()
        //{

        //    using (var context = new CBContext())
        //    {
        //        return context.Contacts.ToList();
        //    }
        //}
        public int GetContactCount(string search)
        {
            using (var context = new CBContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Contacts.Where(contact => contact.FullName != null &&
                    contact.FullName.ToLower().Contains(search.ToLower())).Count();
                }
                else
                {
                    return context.Categories.Count();

                }
            }
        }
        //public List<Contact> GetContactlist(string search, int pageNo, int pagesize)
        //{
        //    using (var context = new CBContext())
        //    {
        //        if (!string.IsNullOrEmpty(search))
        //        {
        //            return context.Contacts.Where(contact => contact.FullName != null &&
        //            contact.FullName.ToLower().Contains(search.ToLower()))
        //            .OrderBy(x => x.Id)
        //            .Skip((pageNo - 1) * pagesize)
        //            .Take(pagesize)
        //            .ToList();
        //        }
        //        else
        //        {
        //            return context.Contacts
        //           .OrderBy(x => x.Id)
        //           .Skip((pageNo - 1) * pagesize)
        //           .Take(pagesize)
        //           .ToList();

        //        }
        //    }
        //}
        //public List<Category> GetShopCategory()
        //{
        //    using (var context = new CBContext())
        //    {
        //        return context.Categories.Where(x => x.IsFeatured && x.ImageUrl != null).ToList();
        //    }
        //}
        //public void CreateCategory(Category category)
        //{
        //    using (var context = new CBContext())
        //    {
        //        context.Categories.Add(category);
        //        context.SaveChanges();
        //    }
        //}
        //public void UpdateContact(Contact contact)
        //{
        //    using (var context = new CBContext())
        //    {
        //        context.Entry(contact).State = System.Data.Entity.EntityState.Modified;
        //        context.SaveChanges();
        //    }
        //}

        public Contact GetContactbyID(int ID)
        {
            using (var context = new CBContext())
            {
               
                return context.Contacts.Where(x => x.Id == ID).FirstOrDefault();

            }

        }

        public void DeleteContact(int ID)
        {
            using (var context = new CBContext())
            {
                var contact = context.Contacts.Find(ID);
                context.Entry(contact).State = System.Data.Entity.EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}
