namespace Book_Shop.database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateContactclass1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "Contact_Id", c => c.Int());
            CreateIndex("dbo.Contacts", "Contact_Id");
            AddForeignKey("dbo.Contacts", "Contact_Id", "dbo.Contacts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contacts", "Contact_Id", "dbo.Contacts");
            DropIndex("dbo.Contacts", new[] { "Contact_Id" });
            DropColumn("dbo.Contacts", "Contact_Id");
        }
    }
}
