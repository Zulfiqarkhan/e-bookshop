namespace Book_Shop.database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addproductattributes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "ISBNno", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Products", "ProductStock", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "ProductStock");
            DropColumn("dbo.Products", "ISBNno");
        }
    }
}
