namespace Book_Shop.database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addpropertiestoproduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Author", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.Products", "Publisher", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Publisher");
            DropColumn("dbo.Products", "Author");
        }
    }
}
