namespace Book_Shop.database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addEditiontoProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Edition", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Edition");
        }
    }
}
