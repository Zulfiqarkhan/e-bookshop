namespace Book_Shop.database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addvalidationatproduct : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "ImageUrl", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "ImageUrl", c => c.String());
        }
    }
}
