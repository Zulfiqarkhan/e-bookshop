// <auto-generated />
namespace Book_Shop.database.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddContactclass : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddContactclass));
        
        string IMigrationMetadata.Id
        {
            get { return "201909140742592_AddContactclass"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
