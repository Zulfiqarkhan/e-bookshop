﻿using Book_Shop.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.database
{
    public class CBDBinitializer : CreateDatabaseIfNotExists<CBContext>
    {
        protected override void Seed(CBContext context)
        {
            SeedRoles(context);
            SeedUsers(context);
        }
        public void SeedRoles(CBContext context)
        {
            List<IdentityRole> RoleInBookshop = new List<IdentityRole>();

            RoleInBookshop.Add(new IdentityRole() { Name = "Administrator" });
            RoleInBookshop.Add(new IdentityRole() { Name = "Moderator" });
            RoleInBookshop.Add(new IdentityRole() { Name = "User" });

            var rolesStore = new RoleStore<IdentityRole>(context);
            var rolesManager = new RoleManager<IdentityRole>(rolesStore);

            foreach (IdentityRole role in RoleInBookshop)
            {
                if (!rolesManager.RoleExists(role.Name))
                {
                    var result = rolesManager.Create(role);
                    if (result.Succeeded) continue;
                }
            }
        }

        public void SeedUsers(CBContext context)
        {
            var userStore = new UserStore<BookshopUser>(context);
            var usersManager = new UserManager<BookshopUser>(userStore);

            BookshopUser admin = new BookshopUser();

            admin.Email = "admin@gmail.com";
            admin.UserName = "admin";
            var password = "admin123";

            if (usersManager.FindByEmail(admin.Email) == null)
            {
                var result = usersManager.Create(admin, password);
                if (result.Succeeded)
                {
                    usersManager.AddToRole(admin.Id, "Administrator");
                    usersManager.AddToRole(admin.Id, "Moderator");
                    usersManager.AddToRole(admin.Id, "User");
                }
            }
        }
    }
}
