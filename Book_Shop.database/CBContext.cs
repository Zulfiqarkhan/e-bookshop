﻿using Book_Shop.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_Shop.database
{
    public class CBContext : IdentityDbContext<BookshopUser>, IDisposable
    {
        public CBContext() : base("BookShopConnection")
        {
            Database.SetInitializer<CBContext>(new CBDBinitializer());
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Config> Configurations { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public  DbSet<Contact> Contacts { get; set; }

        public static CBContext Create()
        {
            return new CBContext();
        }
    }
}
